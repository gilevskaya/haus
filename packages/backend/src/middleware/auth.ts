import * as cookieParser from "cookie-parser";
import * as jsonwebtoken from "jsonwebtoken";
import { UserDB } from "../database";

export const authMiddlewares = [
  cookieParser(),
  (req: any, _res: any, next: any) => {
    let jwt;

    if (
      req.headers.authorization &&
      req.headers.authorization.split(" ")[0] === "jwt"
    ) {
      jwt = req.headers.authorization.split(" ")[1];
    } else if (req.cookies.jwt) {
      jwt = req.cookies.jwt;
    } else {
      return next();
    }

    jsonwebtoken.verify(
      jwt,
      process.env.AUTH_SECRET || "authsecret",
      async (error: any, decoded: any) => {
        if (error) return next(error);
        try {
          const userdb = await UserDB.findOne({ id: decoded.sub });
          if (!userdb) return next();
          req.user = userdb; // adding user to req when logged in
        } catch (userFindError) {
          return next(userFindError);
        }
        return next();
      }
    );
  }
];
