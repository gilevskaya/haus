import * as express from "express";
import logger from "typed-logger";

export const errorMiddleware = () => {
  return (
    error: Error,
    _req: express.Request,
    res: express.Response,
    _next: any
  ) => {
    if (error instanceof Error) {
      logger.warn(error);
      res.status(400);
      res.json({ message: error.message });
    } else {
      logger.error(error);
      res.status(500);
      res.json({ message: "Internal Server Error" });
    }
  };
};
