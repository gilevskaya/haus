import * as React from 'react';

export class BasicLayout extends React.Component<any> {
  public render() {
    const Page = this.props.page;
    return (
      <div>
        <Page />
      </div>
    );
  }
}
