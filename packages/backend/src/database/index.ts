import "reflect-metadata";
import { createConnection } from "typeorm";
import logger from "typed-logger";

// synchronize: true,

createConnection()
  .then(async () => {
    logger.info("Connected to database");
  })
  .catch((error: Error) => {
    logger.fatal(`Error creating connection ${error}`);
  });

export { User as UserDB } from "./entity/User";
export { Feedback as FeedbackDB } from "./entity/Feedback";
