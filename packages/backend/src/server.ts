import * as path from "path";
import * as express from "express";
import * as dotenv from "dotenv";
import logger from "typed-logger";
import * as bodyParser from "body-parser";

if (process.env.NODE_ENV === "development") dotenv.config();
import "./database";
import { authMiddlewares } from "./middleware/auth";
import { errorMiddleware } from "./middleware/error";

import { baseRouter } from "./routers/base";

const app = express();

app.disable("x-powered-by");
app.set("view engine", "ejs");
app.set("json spaces", 2);
app.set("views", path.join(__dirname, "../../views"));

app.use("/api", bodyParser.json(), ...authMiddlewares, baseRouter);

app.use(errorMiddleware());

const port = process.env.PORT || 3010;
const server = app.listen(port, () => {
  server.keepAliveTimeout = 0;
  logger.info(`Server listening on port ${port}`);
});
