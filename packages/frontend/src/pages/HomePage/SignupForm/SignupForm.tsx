import * as React from "react";
import * as cookie from "js-cookie";
import axios from "axios";
import history from "../../../utils/history";

export class SignupForm extends React.Component {
  public state = { form: { name: "", email: "", password: "" }, error: "" };

  public render() {
    return (
      <div className="w-full bg-white rounded-lg max-w-xs px-6">
        <form className="flex flex-col items-center">
          <div className="text-3xl font-hairline text-primary my-8 h-12">
            Create an account
          </div>

          <div className="w-full my-2">
            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2">
              Full Name
            </label>
            <input
              className="appearance-none block w-full bg-grey-lighter text-grey-darker rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="John Doe"
              value={this.state.form.name}
              onChange={this.onSignupInputChange("name")}
            />
          </div>

          <div className="w-full my-2">
            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2">
              Email
            </label>
            <input
              className="appearance-none block w-full bg-grey-lighter text-grey-darker rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="myemail@email.com"
              value={this.state.form.email}
              onChange={this.onSignupInputChange("email")}
            />
          </div>

          <div className="w-full my-2">
            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2">
              Password
            </label>
            <input
              className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-grey"
              type="text"
              placeholder="********"
              value={this.state.form.password}
              onChange={this.onSignupInputChange("password")}
            />
          </div>

          <div className="my-8">
            <button
              className="btn-primary"
              type="button"
              onClick={this.onSignupClick()}
            >
              Sign up
            </button>
          </div>
        </form>
      </div>
    );
  }

  private onSignupInputChange(key: string) {
    return (event: any) => {
      const form = { ...this.state.form };
      form[key] = event.target.value;
      this.setState({ form });
    };
  }

  private onSignupClick() {
    return async () => {
      try {
        const userResponse = await axios.post("/api/user", this.state.form);
        if (userResponse.status !== 200)
          throw new Error(`Problem with user sign up`);
        const user = userResponse.data;
        cookie.set("jwt", user.token);
        history.push(`/feedback`);
      } catch (error) {
        this.setState({ signupRrror: error.message });
      }
    };
  }
}
