import * as express from "express";

export const asyncMiddleware = (
  fn: (req: express.Request, res: express.Response, next: any) => any
) => (req: express.Request, res: express.Response, next: any) => {
  Promise.resolve(fn(req, res, next)).catch(next);
};
