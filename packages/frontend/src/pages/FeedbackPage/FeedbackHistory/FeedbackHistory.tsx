import * as React from "react";
import axios from "axios";
import { IUser } from "../FeedbackPage";

export class FeedbackHistory extends React.Component<{ user: IUser }> {
  public state: {
    allFeedback: Array<{ id: number; text: string; createdAt: string }>;
  } = { allFeedback: [] };

  public async componentWillMount() {
    try {
      const feedbackResponse = await axios.post("/api/feedback/all", {
        user: this.props.user
      });
      if (feedbackResponse.status !== 200)
        throw new Error("Something went wrong while getting feedback history");
      this.setState({ allFeedback: feedbackResponse.data });
    } catch (error) {
      this.setState({ error: error.message });
    }
  }

  public render() {
    return (
      <div className="w-full max-w-md px-6">
        {this.state &&
          this.state.allFeedback &&
          this.state.allFeedback.map(feedbackItem => {
            return (
              <div className="my-8" key={feedbackItem.id}>
                <div className="mb-2 font-bold text-primary text-sm">
                  {feedbackItem.createdAt.toString().slice(0, 10)}
                </div>
                <div className="font-light">{feedbackItem.text}</div>
              </div>
            );
          })}
      </div>
    );
  }
}
