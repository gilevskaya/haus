import { Column, Entity } from "typeorm";
import { MyBaseEntity } from "./MyBaseEntity";

@Entity()
export class User extends MyBaseEntity {
  @Column({ unique: true })
  email: string;

  @Column()
  name: string;

  @Column()
  password: string;
}
