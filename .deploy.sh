# sh .deploy.sh
docker system prune

docker stop $(docker ps -q --filter ancestor=registry.gitlab.com/gilevskaya/haus/frontend:master )
docker pull registry.gitlab.com/gilevskaya/haus/frontend:master
docker run --name haus-frontend -d \
  --restart=always \
  -p 3010:3000 \
  registry.gitlab.com/gilevskaya/haus/frontend:master

docker stop $(docker ps -q --filter ancestor=registry.gitlab.com/gilevskaya/haus/backend:master )
docker rm haus-backend
docker pull registry.gitlab.com/gilevskaya/haus/backend:master
docker run --name haus-backend -d \
  --restart=always \
  -e TYPEORM_CONNECTION = postgres \
  -e TYPEORM_HOST = postgres \
  -e TYPEORM_PORT = 5432 \
  -e TYPEORM_USERNAME = postgres \
  -e TYPEORM_PASSWORD = "" \
  -e TYPEORM_DATABASE = "haus" \
  -e TYPEORM_ENTITIES = "src/database/entity/**/*.ts" \
  -e TYPEORM_SYNCHRONIZE = true \
  --link postgres:postgres \
  -p 3011:3010 \
  registry.gitlab.com/gilevskaya/haus/backend:master
