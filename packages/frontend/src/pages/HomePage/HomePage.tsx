import * as React from "react";
import { Header } from "../../components/Header/Header";
import { SignupForm } from "./SignupForm/SignupForm";

export class HomePage extends React.Component {
  public render() {
    return (
      <div id="home-page">
        <Header authButton="login" background="transparent" />

        <div className="hero">
          <div className="container flex items-center justify-center content-center pb-8">
            <SignupForm />
          </div>
        </div>
      </div>
    );
  }
}
