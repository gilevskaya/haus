# Feedback App

<My task explanation goes here>

There are two packages for the front-end and backend, they have to run in parallel for the app to work.

App also works with Postgres database.

There is basic Gitlab CI pipeline that runs .build.sh after any commit.


Frontend default port is 3000

Backend default port is 3010

## Prod Env

Build and Deploy scripts are included in the root.

Running build.sh script would push images to the container registry.

Then running .deploy.sh would roll it out on the target VM.


TBD: add env variables for the Postgres connection in .deploy.sh

## Dev Env

To work with the app locally you can run the app directly or use docker.

Latter is straight forward, so next follow some explanation on the first one.

### Requirements 
* Node.js v11.2.0
* Yarn
* Webpack

Before running backend, some env variables should be set for the Postgres connection and auth. Adding .env file with them in the root of the backend package would be a good option.
```
AUTH_SECRET=myauthsecret (optional)

TYPEORM_HOST = localhost
TYPEORM_PORT = 5432
TYPEORM_USERNAME = alena
TYPEORM_PASSWORD = ""
TYPEORM_DATABASE = template1
```

Those two commands inside either backend and frontend would run them in the watch mode. You can also specify the PORT variable to both.
```shell
yarn 
yarn start
```

Before testing you have to setup nginx so that :3000 and /api:3010 would forward to the same port.
