import * as React from "react";
import * as cookie from "js-cookie";
import axios from "axios";
import history from "../../../utils/history";

export class LoginForm extends React.Component {
  public state = { form: { email: "", password: "" }, error: "" };

  public render() {
    return (
      <div className="w-full max-w-xs px-6">
        <form className="flex flex-col items-center">
          <div className="text-3xl font-hairline text-primary my-8 h-12">
            Welcome back
          </div>

          <div className="w-full my-2">
            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2">
              Email
            </label>
            <input
              className="appearance-none block w-full bg-grey-lighter text-grey-darker rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              type="text"
              placeholder="myemail@email.com"
              value={this.state.form.email}
              onChange={this.onLoginInputChange("email")}
            />
          </div>

          <div className="w-full my-2">
            <label className="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2">
              Password
            </label>
            <input
              className="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-grey"
              type="password"
              placeholder="********"
              value={this.state.form.password}
              onChange={this.onLoginInputChange("password")}
            />
          </div>

          <div className="mt-8">
            <button
              className="btn-primary"
              type="button"
              onClick={this.onLoginClick()}
            >
              Log In
            </button>
          </div>
        </form>
      </div>
    );
  }

  private onLoginInputChange(key: string) {
    return (event: any) => {
      const form = { ...this.state.form };
      form[key] = event.target.value;
      this.setState({ form });
    };
  }

  private onLoginClick() {
    return async () => {
      try {
        const userResponse = await axios.post(
          "/api/user/login",
          this.state.form
        );
        if (userResponse.status !== 200)
          throw new Error(`Problem with user log in`);
        const user = userResponse.data;
        cookie.set("jwt", user.token);
        history.push(`/feedback`);
      } catch (error) {
        this.setState({ signupRrror: error.message });
      }
    };
  }
}
