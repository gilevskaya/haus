import * as React from "react";
import * as ReactDom from "react-dom";
import history from "./utils/history";
import registerServiceWorker from "./utils/registerServiceWorker";
import { BasicLayout } from "./layouts/BasicLayout/BasicLayout";
import { HomePage } from "./pages/HomePage/HomePage";
import { LoginPage } from "./pages/LoginPage/LoginPage";
import { FeedbackPage } from "./pages/FeedbackPage/FeedbackPage";

import "./index.css";

export interface IAppProps {
  pathname: string;
}

export interface IAppState {
  pathname: string;
}

function getLayout(pathname: string) {
  switch (pathname) {
    default:
      return BasicLayout;
  }
}

const getHandler = (pathname: string) => {
  switch (pathname) {
    case "/":
      return HomePage;
    case "/login":
      return LoginPage;
    case "/feedback":
      return FeedbackPage;

    default:
      return HomePage; // ErrorPage 404 should be here
  }
};

export class App extends React.Component<IAppProps, IAppState> {
  constructor(props: IAppProps) {
    super(props);
    this.state = {
      pathname: props.pathname
    };
  }

  public componentDidMount() {
    history.onChange((pathname: string) => {
      this.setState({ pathname });
    });
  }

  public render() {
    const Layout = getLayout(this.state.pathname);
    const Handler = getHandler(this.state.pathname);

    return <Layout page={Handler} />;
  }
}

ReactDom.render(<App pathname={location.pathname} />, document.getElementById(
  "root"
) as HTMLElement);
registerServiceWorker();
