# sh .build.sh
docker build -t registry.gitlab.com/gilevskaya/haus/frontend:master ./packages/frontend
docker push registry.gitlab.com/gilevskaya/haus/frontend:master
docker build -t registry.gitlab.com/gilevskaya/haus/backend:master ./packages/backend
docker push registry.gitlab.com/gilevskaya/haus/backend:master