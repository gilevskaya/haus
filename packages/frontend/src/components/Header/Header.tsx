import * as React from "react";
import Link from "../Link/Link";

interface IHeaderProps {
  authButton: "login" | "signup" | "logout";
  background: "transparent" | "solid";
  // logoutAction?: any; // function for logout
}

export class Header extends React.Component<IHeaderProps> {
  public render() {
    const extraStyle =
      this.props.background === "solid" ? "bg-primary" : "home-header";

    return (
      <div className={`${extraStyle} pin-x pin-t`}>
        <div className="header-menu container flex items-center justify-between flex-wrap">
          <Link
            className="text-white no-underline font-semibold text-xl"
            href="/"
          >
            feedback
          </Link>
          {this.props.authButton === "login" && (
            <Link className="btn-secondary" href="/login">
              Log in
            </Link>
          )}
          {this.props.authButton === "signup" && (
            <Link className="btn-secondary" href="/">
              Sign up
            </Link>
          )}
          {this.props.authButton === "logout" && (
            <Link className="btn-secondary">Log out</Link>
          )}
        </div>
      </div>
    );
  }
}
