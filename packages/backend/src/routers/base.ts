import * as express from "express";
import logger from "typed-logger";
import axios from "axios";
import { asyncMiddleware } from "../middleware/async";
import { IUser, UserModel } from "../models/UserModel";

export const baseRouter = express.Router();

export interface IRequestWithUser extends express.Request {
  user: IUser;
}

baseRouter.get(
  "/me",
  asyncMiddleware(async (req: IRequestWithUser, res) => {
    logger.trace(`userRouter: /me`);
    if (req.user) res.json(req.user);
    else res.json({});
  })
);

baseRouter.get(
  "/test",
  asyncMiddleware(async (_req, res, next) => {
    logger.trace(`baseRouter: /test`);
    try {
      res.json({ ping: "pong" });
    } catch (error) {
      return next(error);
    }
  })
);

// returns IUser with token
baseRouter.post(
  "/user",
  asyncMiddleware(async (req, res, next) => {
    const { name, email, password } = req.body;
    logger.trace(`baseRouter: /user`);
    try {
      if (!email || !password || !name)
        throw new Error(`All fields are required`);

      const regUserInfo: IUser = { name, email, password };
      const registerRes: IUser = await UserModel.addUser(regUserInfo);
      res.cookie("'jwt'", registerRes.token, { httpOnly: true });
      res.json(registerRes);
    } catch (error) {
      return next(error);
    }
  })
);

// returns IUser with token
baseRouter.post(
  "/user/login",
  asyncMiddleware(async (req, res) => {
    logger.trace(`userRouter: /user/login`);
    const { email, password } = req.body;
    if (!email) throw new Error("Email is missing");
    if (!password) throw new Error("Password is missing");

    const loginRes: IUser = await UserModel.login(email, password);
    res.cookie("jwt", loginRes.token, { httpOnly: true });
    res.json(loginRes);
  })
);

const SLACK_URL =
  "https://hooks.slack.com/services/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";

baseRouter.post(
  "/feedback",
  asyncMiddleware(async (req: any, res) => {
    logger.trace(`userRouter: /feedback`);
    const { text, user } = req.body;
    if (!text || !user.id || !user.email || !user.name)
      throw new Error("Missing params in feedback");

    await UserModel.saveFeedback(user.id, text);

    const slackResponse = await axios.post(SLACK_URL, {
      text: `*${user.name}* _(${user.email})_\n${text}`
    });
    if (slackResponse.status !== 200)
      throw new Error("Something went wrong while sending to slack");

    res.json({
      feedback: text
    });
  })
);

baseRouter.post(
  "/feedback/all",
  asyncMiddleware(async (req: any, res) => {
    logger.trace(`userRouter: /feedback`);
    const { user } = req.body;
    if (!user.id || !user.email || !user.name)
      throw new Error("Missing params in feedback");
    const allFeedback = await UserModel.getFeedback(user.id);
    res.json(allFeedback);
  })
);
