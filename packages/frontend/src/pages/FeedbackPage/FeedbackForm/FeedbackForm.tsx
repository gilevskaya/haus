import * as React from "react";
import axios from "axios";
import { IUser } from "../FeedbackPage";

export class FeedbackForm extends React.Component<{ user: IUser }> {
  public state = { feedbackText: "" };

  public render() {
    return (
      <div className="w-full max-w-md px-6">
        <form>
          <div className="text-3xl font-hairline text-primary my-8 h-12">
            {this.props.user.name}, tell us what you think
          </div>
          <div className="w-full my-2">
            <textarea
              className="appearance-none block w-full bg-grey-lighter text-grey-darker rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
              placeholder="Write your feedback here"
              rows={6}
              onChange={this.onFeedbackChange()}
            />
          </div>
          <div className="my-8">
            <button
              className="btn-primary"
              type="button"
              onClick={this.onFeedbackSend()}
            >
              Leave feedback
            </button>
          </div>
        </form>
      </div>
    );
  }

  private onFeedbackChange() {
    return (event: any) => {
      this.setState({ feedbackText: event.target.value });
    };
  }

  private onFeedbackSend() {
    return async () => {
      try {
        const feedbackResponse = await axios.post("/api/feedback", {
          text: this.state.feedbackText,
          user: this.props.user
        });
        if (feedbackResponse.status !== 200)
          throw new Error("Something went wrong while sending feedback");
      } catch (error) {
        this.setState({ error: error.message });
      }
    };
  }
}
