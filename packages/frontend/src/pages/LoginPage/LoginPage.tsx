import * as React from "react";
import { Header } from "../../components/Header/Header";
import { LoginForm } from "./LoginForm/LoginForm";

export class LoginPage extends React.Component {
  public render() {
    return (
      <div>
        <Header authButton="signup" background="solid" />
        <div className="container flex flex-col items-center justify-center content-center my-8">
          <LoginForm />
        </div>
      </div>
    );
  }
}
