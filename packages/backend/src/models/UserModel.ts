import * as jwt from "jsonwebtoken";
import * as bcrypt from "bcrypt";
import logger from "typed-logger";
import { UserDB, FeedbackDB } from "../database";

const TOKEN_EXPIRATION = 60 * 60 * 24 * 30;
const SALT_ROUNDS = parseInt(process.env.SALT_ROUNDS || "10");

export interface IUser {
  id?: number;
  name: string;
  email: string;
  password?: string;
  token?: string;
}
export interface IJwtToken {
  token: string;
}

export class UserModel {
  public static removePassword(user: UserDB): IUser {
    delete user.password;
    return user;
  }

  public static async login(email: string, password: string): Promise<IUser> {
    const userdb = await UserDB.findOne({ where: { email } });
    if (!userdb) throw new Error("No user with this email");
    const valid = await bcrypt.compare(password, userdb.password);
    if (!valid) throw new Error("Wrong password for this email");

    const user: IUser = this.removePassword(userdb);
    return this.addTokenToUser(user);
  }

  public static async addUser(userInfo: IUser): Promise<IUser> {
    const extistingEmailUser = await UserDB.findOne({
      email: userInfo.email
    });
    if (extistingEmailUser)
      throw new Error("User with this email already exists");

    const hashedPassword = await bcrypt.hash(userInfo.password, SALT_ROUNDS);
    const userdb = await UserDB.create({
      email: userInfo.email,
      password: hashedPassword,
      name: userInfo.name
    });
    await userdb.save();

    const user: IUser = this.removePassword(userdb);
    return this.addTokenToUser(user);
  }

  public static async saveFeedback(userId: number, feedbackText: string) {
    const user = await UserDB.findOne({
      id: userId
    });
    const feedbackdb = await FeedbackDB.create({
      text: feedbackText,
      user
    });
    await feedbackdb.save();
    return feedbackdb;
  }

  public static async getFeedback(userId: number) {
    const user = await UserDB.findOne({
      id: userId
    });
    const userFeedback = await FeedbackDB.find({
      user
    });
    return userFeedback;
  }

  private static addTokenToUser(user: IUser): IUser & IJwtToken {
    logger.debug("addTokenToUser", user);
    const token = jwt.sign(
      { sub: user.id },
      process.env.AUTH_SECRET || "authsecret",
      {
        expiresIn: TOKEN_EXPIRATION
      }
    );
    return { ...user, token };
  }
}
