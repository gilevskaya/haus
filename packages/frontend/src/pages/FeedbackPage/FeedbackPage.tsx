import * as React from "react";
import { Header } from "../../components/Header/Header";
import { FeedbackForm } from "./FeedbackForm/FeedbackForm";
// import logger from "typed-logger";
import axios from "axios";
import { FeedbackHistory } from "./FeedbackHistory/FeedbackHistory";

export interface IUser {
  id: number;
  name: string;
  email: string;
}

export class FeedbackPage extends React.Component {
  public state: { user: IUser };

  public async componentWillMount() {
    const meRes = await axios.get("/api/me");
    if (meRes.data) {
      const { id, name, email } = meRes.data;
      this.setState({ user: { id, name, email } });
    }
    // else : TBD when not logged in user goes to this page
  }

  public render() {
    return (
      <div>
        <Header authButton="logout" background="solid" />
        <div className="hero">
          <div className="container flex flex-col items-center justify-center content-center my-8">
            {this.state && this.state.user && (
              <FeedbackForm user={this.state.user} />
            )}
            {this.state && this.state.user && (
              <FeedbackHistory user={this.state.user} />
            )}
          </div>
        </div>
      </div>
    );
  }
}
