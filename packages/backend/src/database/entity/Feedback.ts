import { Column, Entity, ManyToOne } from "typeorm";
import { MyBaseEntity } from "./MyBaseEntity";
import { User } from "./User";

@Entity()
export class Feedback extends MyBaseEntity {
  @Column()
  text: string;

  @ManyToOne(_type => User, user => user.id)
  public user: User;
}
